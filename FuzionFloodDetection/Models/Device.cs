﻿using System;

namespace FuzionFloodDetection.Models
{
    public class Device
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }

        public Device()
        {

        }

        public static Device DeviceFromCSV(string[] aCSVRow)
        {
            Device aDevice = new Device();

            aDevice.ID = int.Parse(aCSVRow[0]);
            aDevice.Name = aCSVRow[1];
            aDevice.Location = aCSVRow[2];

            return aDevice;
        }

    }
}
