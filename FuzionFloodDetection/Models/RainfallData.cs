﻿using System;
namespace FuzionFloodDetection.Models
{
    public class RainfallData
    {
        public int DeviceID;
        public string Time;
        public float Rainfall;

        public RainfallData()
        {
        }

        public static RainfallData DataFromCSV(string[] aCSVRow)
        {
            RainfallData rainfall = new RainfallData();

            rainfall.DeviceID = int.Parse(aCSVRow[0]);
            rainfall.Time = aCSVRow[1];
            rainfall.Rainfall = float.Parse(aCSVRow[2]);

            return rainfall;
        }
    }
}
